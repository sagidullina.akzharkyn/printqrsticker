
const express        = require('express');
const app            = express();
const multer = require('multer')
const bodyParser = require('body-parser');
const fs = require('fs');
const { crc16ccitt } = require('crc');
const Papa=require('papaparse');
const XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
const xhr = new XMLHttpRequest();
const port = 8080;

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, __dirname+'/uploads')
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now())
    }
  })
var upload = multer({ storage: storage })

app.use(express.static(__dirname+'/public'))
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", function (request, response) {
    response.sendFile(__dirname + "/public/index.html");
});

app.post("/registration", upload.single('csvfile'), function (request, response, next) {
    if(!request.body) return response.sendStatus(400);
    fpath = request.file.path

    var file = '';

    var readStream = fs.createReadStream(fpath);
    readStream.on('data', function(chunk) {  
        file += chunk;
    })
    readStream.on('end', function() {
        console.log(typeof(file));
        function parseFile(file, callBack) {
            Papa.parse(file, {
                header: false,
                complete: (results)=>  {
                    callBack(results.data);
                }
            });
        }
        parseFile(file, (data)=> {
            for(var i = 0; i < data.length; i++){
                var padName = data[i][2].length.toString().padStart(2, '0')
                var padCity = data[i][3].length.toString().padStart(2, '0')
                var plwithoutcrc = `0002010216${data[i][0]}5204${data[i][1]}53033985802KZ59${padName}${data[i][2]}60${padCity}${data[i][3]}6304`
                var crc = (crc16ccitt(plwithoutcrc).toString(16)).toUpperCase()
                var plwithcrc = `${plwithoutcrc}${crc}`
                var xxxx = plwithcrc.length.toString().padStart(4, '0')
                var qrpayload = `MM,B${xxxx}\n${plwithoutcrc}${crc}`
                let label = `^XA\n^GB439,679,1,B,1^FS
                \n^FO236,14^GFA,1118,1118,13,,::::::::::L07IFK01IFE,K07KFI01KFC,J01LFC007LF,J07MF01MFC,I01NF83NF,I07NF11NFC,I0NFE387MFE,003NFC7E3NF8,007NF9FF1NFC,007NF1FF8NFE,00NFE3FF8NFE,01NFC7FFC7NF,03NF8IFE3NF8,07NF9JF3NFC,07NF1JF9NFE,0OF3JF9NFE,0NFE3JF8OF,1NFE7JFCOF,1NFE7JFC7NF,1NFC7JFC7NF,3NFC7JFE7NF83NFCKFE7NF83NFCKFE3NF83NF8KFE3NF8::::3NFCKFE3NF83NFCKFE7NF8:1NFC7JFC7NF,1NFE7JFC7NF,1NFE7JFCOF,0NFE3JF8OF,0OF3JF9NFE,07NF1JF9NFE,07NF9JF3NFC,03NF8IFE3NF8,01NFC7FFC7NF,00NFE3FF8OF,00NFE3FF8NFE,007NF1FF1NFC,003NF8FE3NF8,I0NFC387MFE,I07NF10NFC,I01NF83NF,J07MF01MFE,J01LFC007LF,K07KFI01KFC,L0JF8J03IFE,,:::P02R018,:01B9C0E838FC3C3C3C39161F8,01FFE3FCECFC7E3CFC7F3E7F8,018E330CC060C730C0C330C38,018C320C7C20FF3180C330C18,018C320C0E20C031C0C330C18,018C331C0630C030C0C330E38,018431FCFC3E7E307C7F307F8,,::::::::::^FS
                \n^FO236,14^GFA,1131,1131,13,,::::::::::L03FFEL0IF8,K07KFI01KFC,J01LFC007LF,J07MF01MFC,I01NF83NF,I07NF01NFC,I0NFE38NFE,001NFC7C7NF,003NF8FE3NFC,007NF1FF1NFC,00NFE3FF8NFE,01NFC7FFC7NF,03NFCIFE3NF8,07NF9JF3NFC,07NF1JF1NFC,0OF3JF9NFE,0NFE3JF8NFE,1NFE7JFCOF,:1NFC7JFC7NF,1NFC7JFE7NF83NFCKFE7NF83NFCKFE3NF83NF8KFE3NF8:::::3NFCKFE7NF8:1NFC7JFE7NF81NFC7JFC7NF,1NFE7JFCOF,1NFE3JFCOF,0OF3JF8NFE,0OF3JF9NFE,07NF1JF1NFC,07NF8JF3NFC,03NFCIFE3NF8,01NFC7FFC7NF,00NFE3FF8NFE,007NF1FF1NFC,003NF8FE3NF8,001NFC7C7NF,I0NFE38NFE,I03NF01NFC,I01NF83NF,J07MF01MFC,J01LFC007LF,K03KFJ0KFC,L03FFEL0IF8,,:::P02R018,P06R018,01F9E0FC7CFC3C3C3C3F363F8,01DF73BCECFC6638ECEF3C778,018E330CE020E730C0C330C38,018C320C7C20FF3180C330C18,018C320C0E20C031C0C330C18,018C331C86304030C0E730638,018431FCFC3E7E307C7F303F8,,::::::::::^FS
                \n^FO100,31^GFA,741,741,13,gKFE:::::::::::::,:::T07E,J03FF00F8F81FF807F,K0FF01F8F87FF80FF,K03F01F1F8IF80FF,K01F03F1F8FC381FF,K01F83F1F1F8001FF,K01F83E1F1F8003FF8,L0F87E3F1FC003EF8,L0F87C3F1FF007CF8,L0F8FC3F0FFC07CF8,L0FCF83E07FE0FCF8,L07DF83E01FF0F87C,L07DF07E007F0F87C,L07FF07E003F1IFC,L03FF07C001F1IFC,L03FE07C003F3IFE,L03FE07C7IF3IFE,L03FC0FC7FFE7E03E,L01FC0F87FFC7C03E,L01F80F87FF0FC01F,S03,,:::gKFE:::::::::::::^FS
                \n^FO54,133\n^FB332,2,0,C\n^AQN,12,14\n^FD${data[i][2]}\n^FS
                \n^FO55,179^BQN,2,8\n^FD\n${qrpayload}\n^FS
                \n^FO96,552\n^ARN,18,19^FD\n${data[i][0]}\n^FS
                \n^FO94,583^GFA,3296,3296,32,,::::::::::::::::::::::::::::::P01EgG078S0F,:::::P01E1EI078J0E01CI0F00787I03CJ0E00F,P01E7F803FF00F3F87F003FC079FC01FF81E3FC0F03C,P01EFFC07FFC0F7FCFF80IF07IF03FFC1EFFC0F07C,P01IFC0IFC0LFC1IF07IF03FFC1IFE0F0F8,P01FC7C1F83E0FE3FC7C1E0F87E0F8183E1FE3E0F0F,P01F03E1F01F0FC1F83C3C0787C078001E1F81E0F1E,P01F03E3E00F0F81F03C3C0787C07C001E1F01E0F3E,P01E03E3C00F0F81F03C3IFC7803C03FE1F01E0FFC,P01E03E3C00F8F01E03C7IFC7803C1FFE1F01E0FFC,P01E03E3C00F8F01E03C7IF87803C3FFE1F01E0FFC,P01E03E3C00F0F01E03C3CI07803C7C1E1F01E0F3E,P01E03E3E00F0F01E03C3CI07C07CF81E1F01E0F1F,P01E03E1E01F0F01E03C3EI07C078F03E1F01E0F0F,P01E03E1F03E0F01E03C3F0307E0F8F83E1F01E0F0F8,P01E03E0IFE0F01E03C1IF87FBF87C7E1F01E0F078,P01E03E07FFC0F01E03C0IF87IF07FFE1F01E0F07C,P01E03E03FF80F01E03C07FF07BFE03FDE1F01E0F03E,P01E03E00FE00F01E03C01FC078F801F1E0F01E0F01E,,::::::::::Y01E03E0381C3003C007E030E18,Y03F07F0381C3003E007F070638,Y0310E203C1E30066006307833,Y0300C006C1F3003600638D836,Y03E1C006C1BB003C00638DC1E,Y01F1C00C619B0079807F1CC1C,g038C00FE18F00CF007E1FC0C,Y0218E21FF18F00C700601FE0C,Y07F07F18318700FF00603060C,Y01E03E183983007900603030C,,::::::::::::::::::::::::::^FS
                \n^XZ\n`
                fs.appendFileSync('qrsticker.lbl', label, (err) => {  
                    if (err) throw err;
                });
                console.log(`${i}th label saved!`);
            }
        })
    });
    response.sendFile(__dirname + '/public/index.html')
})

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${port}`)
})

    









// var readedFile = fs.createReadStream(__dirname+ '/qrsticker.lbl', 'utf8')
// var qr = ' ';
// readedFile.on('data', function(chunk) {  
//     qr += chunk;
// }).on('end', function() {
//     var data = JSON.stringify({
//         device: {
//             name: "18j183203678",
//             uid: "18j183203678",
//             connection: "usb",
//             deviceType: "printer",
//             version: 2,
//             provider: "com.zebra.ds.webdriver.desktop.provider.DefaultDeviceProvider",
//             manufacturer:"Zebra Technologies"
//         },
//         data: qr
//     });
//     xhr.open('POST','http://127.0.0.1:9100/write', false)
//     xhr.setRequestHeader('Content-Type', 'text-plain');
//     xhr.send(data)  
// })
//------------------------------------------------------------------//

    // var readedFile = fs.readFileSync('qrsticker.lbl', 'utf8', (err, data) => {
    //     if (err) throw err;
    // });
    // var data = JSON.stringify({
    //     device: {
    //         name: "18j183203678",
    //         uid: "18j183203678",
    //         connection: "usb",
    //         deviceType: "printer",
    //         version: 2,
    //         provider: "com.zebra.ds.webdriver.desktop.provider.DefaultDeviceProvider",
    //         manufacturer:"Zebra Technologies"
    //     },
    //     data: readedFile
    // });
    // xhr.open('POST','http://127.0.0.1:9100/write', false)
    // xhr.setRequestHeader('Content-Type', 'text-plain');
    // xhr.send(data)  
    


    const promiseChaining = (req, res, next) => {
        
    }